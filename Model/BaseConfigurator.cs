﻿using System.Data.Entity.ModelConfiguration;

namespace Lib.Web
{
    /// <summary>
    /// Стандартная конфигурация модели
    /// </summary>
    public class BaseConfigurator<T> : EntityTypeConfiguration<T> where T : BaseModel
    {
        public BaseConfigurator()
        {

        }
    }
}
