﻿using System.ComponentModel.DataAnnotations;

namespace Lib.Web
{
    /// <summary>
    /// Стандартная модель данных
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        [Key]
        public long Id { get; set; }
        
        /// <summary>
        /// Токен для обновления объекта
        /// </summary>
        [ConcurrencyCheck]
        public long UpdateToken { get; set; }
    }
}
