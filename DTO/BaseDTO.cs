﻿namespace Lib.Web
{
    /// <summary>
    /// Стандартный объект для передачи данных
    /// </summary>
    public class BaseDTO
    {
        /// <summary>
        /// Идентификатор объекта
        /// </summary>
        public long Id { get; set; }
        
        /// <summary>
        /// Токен для обновления объекта
        /// </summary>
        public long UpdateToken { get; set; }
    }
}
