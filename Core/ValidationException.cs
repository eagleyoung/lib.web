﻿using System;

namespace Lib.Web
{
    /// <summary>
    /// Ошибка при валидации данных
    /// </summary>
    public class ValidationException : Exception
    {
        /// <summary>
        /// Свойство, не прошедшее валидацию
        /// </summary>
        public string Property { get; }

        public ValidationException(string message, string property) : base(message)
        {
            Property = property;
        }
    }
}
